from django import forms
from .models import Schedule

class ActivitySchedule(forms.Form):
	day = forms.CharField(label="Day", max_length=20)
	date = forms.DateField(widget=forms.DateInput(attrs={'type' : 'date'}))
	time = forms.TimeField(widget=forms.TimeInput(attrs={'type' : 'time'}))
	name = forms.CharField(label="Activity Name", max_length=30, required=True)
	location = forms.CharField(label="Location", max_length=30)
	category = forms.CharField(label="Category", max_length=30)
