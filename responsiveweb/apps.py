from django.apps import AppConfig


class ResponsivewebConfig(AppConfig):
    name = 'responsiveweb'
