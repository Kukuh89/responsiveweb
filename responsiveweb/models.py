from django.db import models

class Schedule(models.Model):
	
	day = models.CharField(max_length=10)
	date = models.DateField()
	time = models.TimeField()
	name = models.CharField(max_length=30)
	location = models.CharField(max_length=30)
	category = models.CharField(max_length=20)


# Create your models here.
