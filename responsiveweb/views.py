from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.template import Context, loader
from django.utils import timezone
import datetime
from .models import Schedule
from .forms import ActivitySchedule

# Create your views here.
def Homepage(request):
    return render(request, 'Homepage.html')
def About(request):
    return render(request, 'About.html')
def Gallery(request):
    return render(request, 'Gallery.html')


def schedule(request):
	form = ActivitySchedule(request.POST or None)
	response = {}
	if(request.method == "POST"):
		if (form.is_valid()):
			day = request.POST.get("day")
			date = request.POST.get("date")
			time = request.POST.get("time")
			name = request.POST.get("name")
			location = request.POST.get("location")
			category = request.POST.get("category")
			Schedule.objects.create(day=day, date=date, time=time, name=name, location=location, category=category)
			return redirect('/Showschedule')
		else:
			response['form'] = form
			return render(request, 'schedule.html', response)
	else:
		response['form'] = form
		return render(request, 'schedule.html', response)


def showschedule(request):
    response = {}
    schedule = Schedule.objects.all()
    response = {
        "schedule" : schedule
    }
    return render(request, 'showschedule.html', response)


def delete(request):
    schedule = Schedule.objects.all().delete()
    return redirect('/Showschedule')


# Create your views here.
