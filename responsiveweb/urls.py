from django.urls import path
from . import views

app_name= 'responsiveweb'

urlpatterns = [
    path('', views.Homepage, name='Homepage'),
    path('About/', views.About, name='About'),
    path('Gallery/', views.Gallery, name='Gallery'),
    
    path('Schedule/', views.schedule, name="schedule"),
	path('Showschedule/', views.showschedule, name="showschedule"),
	path('delete/', views.delete, name="delete"),

    
    

]
